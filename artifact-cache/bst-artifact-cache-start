#!/bin/bash

set -eu

# Configuration settings
CN="${CN:-localhost}"
ENABLE_PUSH="${ENABLE_PUSH:-true}"

KEY_VOLUME="${KEY_VOLUME:-/home/artifacts/keys}"
DATA_VOLUME="${DATA_VOLUME:-/data}"

if [ "$ENABLE_PUSH" == true ]; then
    port=1102
else
    port=1101
fi

# If we lack any key/cert, create it
if ! [ -f "$KEY_VOLUME/server.key" ]; then
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -batch -subj "/CN=$CN" -out "$KEY_VOLUME/server.crt" -keyout "$KEY_VOLUME/server.key"
fi

# Note that an empty authorized.crt file will cause bst-artifact-server command
# to fail; so allow ourself to push if it's empty or non-existent
if [ "$ENABLE_PUSH" == true ]; then
    if [ ! -f "$KEY_VOLUME/authorized.crt" ] || [ ! -s "$KEY_VOLUME/authorized.crt" ]; then
        cp "$KEY_VOLUME/server.crt" "$KEY_VOLUME/authorized.crt"
    fi
fi

args=(
    "--port" "$port"
    "--server-key" "$KEY_VOLUME/server.key"
    "--server-cert" "$KEY_VOLUME/server.crt"
)
if [ "$ENABLE_PUSH" == true ]; then
    args+=(
        "--client-certs" "$KEY_VOLUME/authorized.crt"
        "--enable-push"
    )
fi
args+=("$DATA_VOLUME")

bst-artifact-server "${args[@]}"
