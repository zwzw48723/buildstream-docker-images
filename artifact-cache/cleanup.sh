#!/bin/sh

set -eu

echo "Removing gcc and python3-devel"
dnf remove -y python3-devel

echo "Removing Git"
dnf remove -y git

echo "Removing DNF cache"
dnf clean all

rm -R /root/*
