#!/bin/sh

set -eu

# Buildstream base dependencies
dnf install -y \
        python3 fuse-devel

# Packages to build python dependencies
dnf install -y \
        python3-pip python-setuptools python3-devel gcc-c++

# Git is needed so `pip3 install .` can detect the version number of
# the BuildStream package. We remove it in the cleanup.sh script as
# we save 90MB that way.
dnf install -y git

# Install openssl-devel so we can build grpc from source
dnf install -y openssl-devel

# Install grpc from distro to utilize the optimisations
dnf install -y grpc

# Create the server keys volume
useradd artifacts
mkdir /home/artifacts/keys

# Make an empty user certificates file (users are intended to append
# their keys to this file if push access is to be permitted)
touch /home/artifacts/keys/authorized.crt

# artifacts user must own the keys directory
chown -R artifacts:artifacts /home/artifacts/keys

# Prepare the mount point for the data volume
mkdir /data
chown artifacts:artifacts /data
